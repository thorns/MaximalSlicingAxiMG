
#include "cctk.h"
#include "cctk_Arguments.h"
#include "Slicing.h"

int maximal_slicing_axi_mg_register(void)
{
    Einstein_RegisterSlicing("maximal_axi_mg");
    return 0;
}

void maximal_slicing_axi_mg_register_mol(CCTK_ARGUMENTS)
{
    MoLRegisterConstrained(CCTK_VarIndex("ADMBase::alp"));

    MoLRegisterSaveAndRestoreGroup(CCTK_GroupIndex("ADMBase::metric"));
    MoLRegisterSaveAndRestoreGroup(CCTK_GroupIndex("ADMBase::curv"));
}
